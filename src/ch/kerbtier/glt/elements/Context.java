package ch.kerbtier.glt.elements;

import ch.kerbtier.glt.Layer;

public class Context {
  private Layer layer;

  public Layer getLayer() {
    return layer;
  }

  public void setLayer(Layer layer) {
    this.layer = layer;
  }
}
